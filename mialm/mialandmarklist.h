/*
 *  Copyright (c) 2004-2013 Gert Wollny <gw.fossdev@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  As an exception to this license, "NEC C&C Research Labs" may use
 *  this software under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation.
 *
 */
 
#ifndef __landmarklist_h
#define __landmarklist_h

#include <glib.h>
#include <glib-object.h>

#include <mialm/mialandmark.h>
	

#define MIA_TYPE_LANDMARKLIST (mia_landmarklist_get_type())
#define MIA_LANDMARKLIST(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MIA_TYPE_LANDMARKLIST, MiaLandmarklist))
#define MIA_LANDMARKLIST_CLASS(vtable) (G_TYPE_CHECK_CLASS_CAST((vtable),MIA_TYPE_LANDMARKLIST, MiaLandmarklistClass))
#define MIA_IS_LANDMARKLIST(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),MIA_TYPE_LANDMARKLIST))
#define MIA_IS_LANDMARKLIST_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj),MIA_TYPE_LANDMARKLIST))
#define MIA_LANDMARKLIST_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS((inst), MIA_TYPE_LANDMARKLIST, MiaLandmarklistClass))

typedef struct _MiaLandmarklist MiaLandmarklist;

typedef struct _MiaLandmarklistClass MiaLandmarklistClass;
struct _MiaLandmarklistClass {
	GObjectClass parent;
};

extern const ParserTags landmarklist_parser_tags[];

G_BEGIN_DECLS

GType mia_landmarklist_get_type(void);

MiaLandmarklist *mia_landmarklist_new(const gchar *name);

gboolean
mia_landmarklist_insert(MiaLandmarklist *self, MiaLandmark *lm);

MiaLandmark *
mia_landmarklist_get_landmark(MiaLandmarklist *self, const gchar *key);

void 
mia_landmarklist_delete_landmark(MiaLandmarklist *self, const gchar *key);

void
mia_landmarklist_set_name(MiaLandmarklist *self, const gchar *name);

const gchar *
mia_landmarklist_get_name(MiaLandmarklist *self);

const gchar *mia_landmarklist_get_selected(MiaLandmarklist *self);

void mia_landmarklist_set_selected(MiaLandmarklist *self, const gchar *selected);

void 
mia_landmarklist_foreach(MiaLandmarklist *self, 
			 GHFunc callback, gpointer user_data);

void 
mia_landmarklist_clear_locations(MiaLandmarklist *self);

gboolean 
mia_landmarklist_xmlio_write(xmlNodePtr parent, xmlNsPtr ns, MiaLandmarklist *list);


gboolean mia_landmarklist_test(void);

G_END_DECLS

#endif
